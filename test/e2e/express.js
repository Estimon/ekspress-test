var config = require('../../nightwatch.conf.js');

module.exports = { 
  '@tags': ['express'],
  'express resize windows and click some links': function(browser) {
    browser
      .url('https://ekspress.delfi.ee/')
      .resizeWindow(1920, 1080)
      .waitForElementVisible('body')
      .pause(100)
      .click('a[href="/elu"]')
      .waitForElementVisible('body')
      .pause(100)  
      .click('a[href="/elu/kas-mutimaksakaste-kolbab-suua-kindralmajor-martin-heremi-6-tarkust-muttide-ja-nende-puudmise-kohta?id=85528377"]')
      .waitForElementVisible('body')
      .pause(100) 
      .end();
  },

  'use the search bar test' : function(browser){
      browser
      .url('https://ekspress.delfi.ee/')
      .resizeWindow(1920, 1080)
      .waitForElementVisible('body')
      .pause(100)
      .setValue('input[type="text"]', ['auto', browser.Keys.ENTER])
      .waitForElementVisible('body')
      .pause(300)
      .end();
  },
  'Use the hamburger button' : function(browser){
      browser
      .url('https://ekspress.delfi.ee/')
      .resizeWindow(1920, 1080)
      .waitForElementVisible('body')
      .pause(1000)
      .click('a[class="header__menu-burger toggle-sidebar"]')
      .pause(1000)
      .click('a[class="header__menu-burger toggle-sidebar is-active"')
      .pause(1000)
      .end();
  }
      
       
  

};
